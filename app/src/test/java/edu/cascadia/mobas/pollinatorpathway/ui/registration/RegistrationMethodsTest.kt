package edu.cascadia.mobas.pollinatorpathway.ui.registration

import org.junit.Assert.*

import org.junit.Test

class RegistrationMethodsTest {

    @Test
    fun getAccountInfo() {
        var email = "test@test.com"
        var password = "password"
        var resultEquals = arrayOf("a","b")

        val result = RegistrationMethods.getAccountInfo(email, password)
        assertArrayEquals(resultEquals, result)

    }
}