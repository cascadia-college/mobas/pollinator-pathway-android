package edu.cascadia.mobas.pollinatorpathway.ui.login

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import edu.cascadia.mobas.pollinatorpathway.data.model.Profile
import edu.cascadia.mobas.pollinatorpathway.databinding.ActivityRegisterBinding
import edu.cascadia.mobas.pollinatorpathway.data.database.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.regex.Pattern

class RegisterActivity : AppCompatActivity() {

    private lateinit var binding: ActivityRegisterBinding
    private val coroutineScope = CoroutineScope(Dispatchers.IO)
    private val db by lazy { PpLocalDb.getDatabase(this, coroutineScope) }




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegisterBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.register.setOnClickListener {
            lifecycleScope.launch {
                val email = binding.username.text.toString().trim()
                val password = binding.password.text.toString().trim()
                val confirmPassword = binding.cpassword.text.toString().trim()

                // Check that email is valid and not already in the database
                if (!isValidEmail(email)) {
                    binding.username.error = "Invalid email"
                    binding.username.requestFocus()
                    return@launch
                }

                val emailExists = emailExists(email)
                Log.d("RegisterActivity", "email: $email")
                Log.d("RegisterActivity", "emailExists: $emailExists")
                if (emailExists) {
                    binding.username.error = "Email already in use"
                    binding.username.requestFocus()
                    return@launch
                }

                // Check that password meets minimum requirements and matches confirm password
                if (!isValidPassword(password)) {
                    binding.password.error =
                        "Password must be at least 8 characters with 1 uppercase and 1 special character"
                    binding.password.requestFocus()
                    return@launch
                } else if (password != confirmPassword) {
                    binding.cpassword.error = "Passwords do not match"
                    binding.cpassword.requestFocus()
                    return@launch
                }

                // Add new user to the database
                val profile = Profile(email = email, password = password)
                withContext(Dispatchers.IO) {
                    db.profileDao().insertProfile(profile)
                }
                Toast.makeText(
                    this@RegisterActivity,
                    "User registered successfully",
                    Toast.LENGTH_SHORT
                ).show()
                finish()
            }
        }




    }


    private fun isValidPassword(password: String): Boolean {
        val pattern = Pattern.compile("^(?=.*[A-Z])(?=.*[@#\$%^&+=!])(?=\\S+\$).{8,}\$")
        Log.d("RegisterActivity", "Password regex pattern: ${pattern.pattern()}")
        return pattern.matcher(password).matches()
    }
    private fun isValidEmail(email: String): Boolean {
        val pattern = Pattern.compile("^[\\w_]+([\\.-]?[\\w_]+)*@[a-zA-Z0-9_]+([\\.-]?[\\w_]+)*\\.[a-zA-Z]{2,}(\\.[a-zA-Z]{2,6})?\$")
        return pattern.matcher(email).matches()
    }
    private suspend fun emailExists(email: String): Boolean {
        return withContext(coroutineScope.coroutineContext) {
            db.profileDao().emailExists(email)
        }
    }

}

