package edu.cascadia.mobas.pollinatorpathway.data.network

import edu.cascadia.mobas.pollinatorpathway.data.model.Profile
import io.ktor.client.call.*
import io.ktor.client.request.*
import io.ktor.http.*
import kotlinx.serialization.Serializable

object ProfileAPIServices
{
    private val httpClient by lazy { NetworkClient.getInstance }

    suspend fun singleProfile(id:Long) : Profile {
        val response =  httpClient.get{
            url{
                protocol = URLProtocol.HTTP
                host = "10.0.2.2"
                port = 5107
                appendPathSegments("api","profiles","$id")
            }
        }
        return response.body()
    }
}