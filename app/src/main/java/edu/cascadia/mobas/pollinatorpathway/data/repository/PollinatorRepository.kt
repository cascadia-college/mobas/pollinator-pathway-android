package edu.cascadia.mobas.pollinatorpathway.data.repository

import edu.cascadia.mobas.pollinatorpathway.data.database.PollinatorDao
import edu.cascadia.mobas.pollinatorpathway.data.model.Pollinator
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.withContext

class PollinatorRepository (
    private val pollinatorDao: PollinatorDao){
    fun getPollinator(pollinatorId: Long) : Flow<Pollinator> = pollinatorDao.getPollinator(pollinatorId)
    fun getIPollinators(): Flow<List<Pollinator>> = pollinatorDao.getPollinators()

    suspend fun addPollinator(pollinator: Pollinator): Long {
        return withContext(Dispatchers.IO) {
            pollinatorDao.addPollinator(pollinator)
        }
    }
}