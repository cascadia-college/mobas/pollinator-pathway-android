package edu.cascadia.mobas.pollinatorpathway.ui.browse

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.asLiveData
import edu.cascadia.mobas.pollinatorpathway.data.model.Pollinator
import edu.cascadia.mobas.pollinatorpathway.data.repository.PollinatorRepository

class PollinatorListViewModel(private val repository: PollinatorRepository) : ViewModel() {
    val pollinatorList: LiveData<List<Pollinator>> = repository.getIPollinators().asLiveData()
    fun getPollinators() = repository.getIPollinators()
}
class PollinatorListViewModelFactory(private val repository: PollinatorRepository) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(PollinatorListViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return PollinatorListViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}