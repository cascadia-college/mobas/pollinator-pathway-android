package edu.cascadia.mobas.pollinatorpathway.ui.registration

import android.media.Image
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import edu.cascadia.mobas.pollinatorpathway.R
import edu.cascadia.mobas.pollinatorpathway.databinding.FragmentRegistrationPublishBinding
import edu.cascadia.mobas.pollinatorpathway.ui.registration.RegistrationViewModel

class RegistrationPublishFragment : Fragment() {

    private var _binding: FragmentRegistrationPublishBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val homeViewModel =
            ViewModelProvider(this).get(RegistrationViewModel::class.java)



        _binding = FragmentRegistrationPublishBinding.inflate(inflater, container, false)
        val root: View = binding.root
        val navController = findNavController()

        var Confirm = root.findViewById<Button>(R.id.button)
        var Edit = root.findViewById<Button>(R.id.button7)

        Confirm.setOnClickListener {

            val bundle = Bundle()
            bundle.putString("uploadStatus", "true")
            navController.navigate(R.id.navigation_upload, bundle)
        }
        Edit.setOnClickListener {
//TODO: make nav
            /*val bundle = Bundle()
            bundle.putString("uploadStatus", textView.text.toString())
            navController.navigate(R.id.navigation_addingredients, bundle)*/
        }



//TODO: display error if any

        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}