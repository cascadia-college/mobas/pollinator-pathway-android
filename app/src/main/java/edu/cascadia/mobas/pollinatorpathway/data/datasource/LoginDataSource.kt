package edu.cascadia.mobas.pollinatorpathway.data.datasource

import androidx.room.Dao
import edu.cascadia.mobas.pollinatorpathway.data.database.ProfileDao
import edu.cascadia.mobas.pollinatorpathway.data.model.LoggedInUser
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.IOException

/**
 * Class that handles authentication w/ login credentials and retrieves user information.
 */
class LoginDataSource(private val profileDao: ProfileDao) { //TODO: change this to LoginDao

    suspend fun login(username: String, password: String): Result<LoggedInUser> {
        return try {
            // Handle loggedInUser authentication
            val loggedInUser = getLoggedInUser(username, password)
            Result.Success(loggedInUser)
        } catch (e: Throwable) {
            Result.Error(IOException("Error logging in", e))
        }
    }

    fun logout() {
        // TODO: revoke authentication
    }

    private suspend fun getLoggedInUser(username: String, password: String) : LoggedInUser {
        return withContext(Dispatchers.IO){
            profileDao.getLoggedInUser(username, password)
        }
    }
}