package edu.cascadia.mobas.pollinatorpathway.ui.registration

import android.media.Image
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import edu.cascadia.mobas.pollinatorpathway.R
import edu.cascadia.mobas.pollinatorpathway.databinding.FragmentRegistrationUploadBinding
import edu.cascadia.mobas.pollinatorpathway.ui.registration.RegistrationViewModel

class RegistrationUploadFragment : Fragment() {

    private var _binding: FragmentRegistrationUploadBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val homeViewModel =
            ViewModelProvider(this).get(RegistrationViewModel::class.java)

        val uploadStatus = getArguments()?.getString("uploadStatus").toBoolean()

        _binding = FragmentRegistrationUploadBinding.inflate(inflater, container, false)
        val root: View = binding.root
        val navController = findNavController()

        var uploadText = root.findViewById<TextView>(R.id.textView7)
        var errorText = root.findViewById<TextView>(R.id.textView8)
        var img = root.findViewById<ImageView>(R.id.imageView123)



        if(uploadStatus){
            uploadText.text = "Your planting is Uploaded!"
            img.setImageDrawable(getResources().getDrawable(R.drawable.ok))
            Handler().postDelayed({
                navController.navigate(R.id.navigation_home)
            }, 5000)

        }
        else{
            uploadText.text = "Your planting has not been Uploaded."
            img.setImageDrawable(getResources().getDrawable(R.drawable.notok))
            Handler().postDelayed({
                navController.navigate(R.id.navigation_home)
            }, 5000)
        }
//TODO: display error if any

        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}