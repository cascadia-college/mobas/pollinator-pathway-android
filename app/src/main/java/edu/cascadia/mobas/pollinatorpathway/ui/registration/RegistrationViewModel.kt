package edu.cascadia.mobas.pollinatorpathway.ui.registration

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class RegistrationViewModel() : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "This string is in the HomeViewModel"
    }
    val text: LiveData<String> = _text
}