package edu.cascadia.mobas.pollinatorpathway.data.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import edu.cascadia.mobas.pollinatorpathway.data.model.*

@Database(entities = [Profile::class, Pollinator::class,], version = 1)
abstract class PpLocalDb : RoomDatabase() {
    abstract fun profileDao(): ProfileDao
    abstract fun pollinatorDao(): PollinatorDao
    //abstract fun plantingDao(): PlantingDao
    //abstract fun boxDao(): BoxDao?

    companion object {
        private const val DATABASE_NAME = "user.db"

        @Volatile
        private var INSTANCE: PpLocalDb? = null
        //private val dbExecutor = Executors.newSingleThreadExecutor()

        fun getDatabase(
            context: Context,
            scope: CoroutineScope
        ): PpLocalDb {
            // if the INSTANCE doesn't exists then create it
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    PpLocalDb::class.java,
                    "profile_database"
                )
                    //TODO: Add a migration strategy instead of wiping data
                    .fallbackToDestructiveMigration()
                    .addCallback(ProfileDatabaseCallback(scope))
                    .build()
                INSTANCE = instance
                // otherwise return the existing instance
                instance
            }
        }
        //Define the Callback class using the coroutine scope
        private class ProfileDatabaseCallback(
            private val scope: CoroutineScope
        ) : RoomDatabase.Callback() {
            // Override the onCreate method to populate the database.
            override fun onCreate(db: SupportSQLiteDatabase) {
                super.onCreate(db)
                // If you want to keep the data through app restarts,
                // comment out the following line.
                INSTANCE?.let { database ->
                    scope.launch(Dispatchers.IO) {
                        populateDatabase(database.profileDao(), database.pollinatorDao())
                    }
                }
            }
        }

        // Wipe the database and add test data
        suspend fun populateDatabase(profileDao: ProfileDao, pollinatorDao: PollinatorDao) {
            // Add a dummy profile
            profileDao.deleteAll()
            val p = Profile(
                "21 Acres",
                "Non-profit",
                "The 21 Acres Center for Local Food & Sustainable living is a global leader in sustainable & regenerative practices, serving as a living laboratory & learning center for conscious consumers who want to learn new, more sustainable ways of living.",
                "https://21acres.org/?s=beevesting",
                "https://www.facebook.com/21Acres/",
                "https://www.instagram.com/21acres/",
                "11/2021",
                "12345 Acres street",
                "Woodinville",
                "WA",
                "98031",
                47.750382030525,
                -122.1569490466,
                "Robin",
                "Crowder",
                "326-224-7220",
                "rbcrowder@21acres.org",
                "123Bee")
            profileDao.insertProfile(p)

            val c = Profile("example@example.com", "password")
            profileDao.insertProfile(c)

            // Add pollinator to
            pollinatorDao.addPollinator(Pollinator(name="Bird"))
            pollinatorDao.addPollinator(Pollinator(name="Bat"))
            pollinatorDao.addPollinator(Pollinator(name="Bee"))
            pollinatorDao.addPollinator(Pollinator(name="Butterfly"))
            pollinatorDao.addPollinator(Pollinator(name="Beetle"))
            pollinatorDao.addPollinator(Pollinator(name="Fly"))
            pollinatorDao.addPollinator(Pollinator(name="Moth"))
        }

    }
}