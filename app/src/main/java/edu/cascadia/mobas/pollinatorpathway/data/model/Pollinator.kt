package edu.cascadia.mobas.pollinatorpathway.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Pollinator(
    @PrimaryKey(autoGenerate = true)
    var pollinatorId: Long = 0,

    var name: String,

){

}