package edu.cascadia.mobas.pollinatorpathway.ui.browse

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import edu.cascadia.mobas.pollinatorpathway.R
import edu.cascadia.mobas.pollinatorpathway.data.model.Pollinator
import edu.cascadia.mobas.pollinatorpathway.databinding.PollinatorListItemBinding

class PollinatorListAdapter(private val pollinatorList: List<Pollinator>):
    RecyclerView.Adapter<PollinatorListAdapter.ViewHolder> (){

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val binding = PollinatorListItemBinding.bind(itemView)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.pollinator_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val pollinator = pollinatorList[position]
        with(holder.binding) {
            pollinatorName.text = pollinator.name
            holder.itemView.tag = pollinator.pollinatorId
        }
    }
    override fun getItemCount() = pollinatorList.size
}