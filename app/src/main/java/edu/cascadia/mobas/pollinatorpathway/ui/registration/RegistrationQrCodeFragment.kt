package edu.cascadia.mobas.pollinatorpathway.ui.registration

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.budiyev.android.codescanner.AutoFocusMode
import com.budiyev.android.codescanner.CodeScanner
import com.budiyev.android.codescanner.CodeScannerView
import com.budiyev.android.codescanner.DecodeCallback
import com.budiyev.android.codescanner.ScanMode
import com.google.zxing.BarcodeFormat
import edu.cascadia.mobas.pollinatorpathway.R
import edu.cascadia.mobas.pollinatorpathway.databinding.FragmentRegistrationQrCodeBinding


class QrCodeFragment : Fragment() {

    private lateinit var codeScanner: CodeScanner
    private var _binding: FragmentRegistrationQrCodeBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentRegistrationQrCodeBinding.inflate(inflater, container, false)
        val root: View = binding.root
        val navController = findNavController()

        //val textView: TextView = binding.textView
        //profileViewModel.text.observe(viewLifecycleOwner) {
        //    textView.text = "User profile name"
        //}

        val scannerView = binding.root.findViewById<CodeScannerView>(R.id.scanner_view)
        val activity = requireActivity()



        codeScanner = CodeScanner(activity, scannerView)
        // Parameters (default values)
        codeScanner.camera = CodeScanner.CAMERA_BACK // or CAMERA_FRONT or specific camera id
        codeScanner.formats = listOf(BarcodeFormat.QR_CODE) // list of type BarcodeFormat,
        // ex. listOf(BarcodeFormat.QR_CODE)
        codeScanner.autoFocusMode = AutoFocusMode.SAFE // or CONTINUOUS
        codeScanner.scanMode = ScanMode.SINGLE // or CONTINUOUS or PREVIEW
        codeScanner.isAutoFocusEnabled = true // Whether to enable auto focus or not
        codeScanner.isFlashEnabled = false // Whether to enable flash or not
        codeScanner.decodeCallback = DecodeCallback {
            activity.runOnUiThread {
                val bundle = Bundle()
                bundle.putString("QRCODE", it.text)
                navController.navigate(R.id.navigation_reg_map, bundle)
            }
        }
        scannerView.setOnClickListener {
            codeScanner.startPreview()
        }
        //navController.navigate(R.id.navigation_addingredients, bundle) //TODO: add ingredients



        return root
    }
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
    override fun onResume() {
        super.onResume()
        codeScanner.startPreview()
    }

    override fun onPause() {
        codeScanner.releaseResources()
        super.onPause()
    }
}