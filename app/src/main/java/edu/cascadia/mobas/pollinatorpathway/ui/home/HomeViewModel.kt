package edu.cascadia.mobas.pollinatorpathway.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class HomeViewModel() : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "This string is in the HomeViewModel"
    }
    val text: LiveData<String> = _text
}