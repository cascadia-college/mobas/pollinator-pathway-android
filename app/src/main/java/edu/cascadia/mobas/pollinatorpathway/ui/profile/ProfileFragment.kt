package edu.cascadia.mobas.pollinatorpathway.ui.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import edu.cascadia.mobas.pollinatorpathway.PpApplication
import edu.cascadia.mobas.pollinatorpathway.R
import edu.cascadia.mobas.pollinatorpathway.databinding.FragmentProfileBinding

class ProfileFragment : Fragment() {

    private var _binding: FragmentProfileBinding? = null

    // This property is only valid between onCreateView and onDestroyView.
    private val binding get() = _binding!!
    private val profileViewModel: ProfileViewModel by viewModels {
        ProfileViewModelFactory((requireContext().applicationContext as PpApplication).profileRepository)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentProfileBinding.inflate(inflater, container, false)
        val root: View = binding.root

        profileViewModel.profile.observe(viewLifecycleOwner) {
            binding.orgName.text = it.orgName
            binding.orgDescription.text = it.orgDescription ?: ""
            binding.address.text = "${it.address}\n${it.city} ${it.state} ${it.postalCode}"
            binding.addressCheckBox.isChecked = true;  //TODO: display "Visible to Public" based on db boolean value rather than checkbox
            binding.gps.text = getString(R.string.gps,it.latitude,it.longitude)
            binding.gpsCheckbox.isChecked = false; //TODO: same as above
            binding.facebook.text = it.facebook
            binding.insta.text =  it.instagram
            binding.contactName.text = "${it.firstname} ${it.lastname}"
            binding.email.text = "${it.email}"
            binding.phone.text = "${it.phone}"
        }
        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}