package edu.cascadia.mobas.pollinatorpathway.data.network

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class ProfileDTO(
    @SerialName("id")
    val id:Long,

    @SerialName("email")
    val email: String,

    @SerialName("firstname")
    val firstname: String,

    @SerialName("lastname")
    val lastName: String,
)