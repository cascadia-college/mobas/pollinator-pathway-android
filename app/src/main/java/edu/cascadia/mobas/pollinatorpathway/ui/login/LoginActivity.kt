package edu.cascadia.mobas.pollinatorpathway.ui.login

import android.app.Activity
import android.content.Intent
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.Toast
import edu.cascadia.mobas.pollinatorpathway.MainActivity
import edu.cascadia.mobas.pollinatorpathway.PpApplication
import edu.cascadia.mobas.pollinatorpathway.databinding.ActivityLoginBinding

import edu.cascadia.mobas.pollinatorpathway.R

class LoginActivity : AppCompatActivity() {

    private lateinit var loginViewModel: LoginViewModel
    private lateinit var binding: ActivityLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Instantiate LoginRepository
        val loginRepository = (application as PpApplication).loginRepository

        // Instantiate LoginViewModel with LoginRepository
        loginViewModel = ViewModelProvider(this, LoginViewModelFactory(loginRepository)).get(LoginViewModel::class.java)

        // Get references to UI components
        val username = binding.username
        val password = binding.password
        val login = binding.login
        val loading = binding.loading

        // Register button listener
        binding.register?.setOnClickListener {
            val intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)
        }

        // Login button listener
        login.setOnClickListener {
            loading.visibility = View.VISIBLE
            loginViewModel.login(username.text.toString(), password.text.toString())
        }

        // Observe the LoginResult LiveData to handle login success or failure
        loginViewModel.loginResult.observe(this, Observer { result ->
            loading.visibility = View.GONE
            if (result.error != null) {
                showLoginFailed(result.error)
            } else if (result.success != null) {
                startMainActivity(result.success)
                setResult(Activity.RESULT_OK)
                finish()
            }
        })

        // Observe the LoginFormState LiveData to enable/disable the login button
        loginViewModel.loginFormState.observe(this, Observer { formState ->
            login.isEnabled = formState.isDataValid
            if (formState.usernameError != null) {
                username.error = getString(formState.usernameError)
            }
            if (formState.passwordError != null) {
                password.error = getString(formState.passwordError)
            }
        })

        // Set up TextChangedListeners to update the LoginFormState LiveData
        username.afterTextChanged {
            loginViewModel.loginDataChanged(
                username.text.toString(),
                password.text.toString()
            )
        }

        password.apply {
            afterTextChanged {
                loginViewModel.loginDataChanged(
                    username.text.toString(),
                    password.text.toString()
                )
            }

            setOnEditorActionListener { _, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    loginViewModel.login(
                        username.text.toString(),
                        password.text.toString()
                    )
                }
                false
            }
        }
    }

    private fun startMainActivity(model: LoggedInUserView) {
        // Add a welcome Toast if View exists
        model.email?.let {
            val welcome = getString(R.string.welcome)
            Toast.makeText(
                applicationContext,
                "$welcome $it",
                Toast.LENGTH_LONG
            ).show()
        }
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }

    private fun showLoginFailed(@StringRes errorString: Int) {
        Toast.makeText(applicationContext, errorString, Toast.LENGTH_SHORT).show()
    }
}


/**
 * Extension function to simplify setting an afterTextChanged action to EditText components.
 */
fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(editable: Editable?) {
            afterTextChanged.invoke(editable.toString())
        }

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
    })
}