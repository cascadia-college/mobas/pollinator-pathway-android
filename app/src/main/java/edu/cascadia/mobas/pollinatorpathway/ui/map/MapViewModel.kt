package edu.cascadia.mobas.pollinatorpathway.ui.map

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MapViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "This string is in the MapViewModel"
    }
    val text: LiveData<String> = _text
}