package edu.cascadia.mobas.pollinatorpathway.data.network

import io.ktor.client.*
import io.ktor.client.engine.android.*
import io.ktor.client.plugins.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.serialization.kotlinx.json.*
import io.ktor.client.request.*
import kotlinx.serialization.json.Json

object NetworkClient {
    //Reference: https://blog.devgenius.io/out-with-retrofit-and-in-with-ktor-client-e8b52f205139
    private val AUTHORIZATION_HEADER = "Authorization"
    private var API_KEY: String = "TESTING"

    private val client = HttpClient(Android){
        defaultRequest {
            header(AUTHORIZATION_HEADER, "BEARER $API_KEY")
        }
        install(ContentNegotiation){
            json(Json {
                isLenient = true
                ignoreUnknownKeys = true
                explicitNulls = false
            })
        }
    }
    val getInstance = client
}