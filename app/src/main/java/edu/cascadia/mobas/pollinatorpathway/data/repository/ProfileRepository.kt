package edu.cascadia.mobas.pollinatorpathway.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import edu.cascadia.mobas.pollinatorpathway.data.database.ProfileDao
import edu.cascadia.mobas.pollinatorpathway.data.model.Profile
import edu.cascadia.mobas.pollinatorpathway.data.network.ProfileAPIServices
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ProfileRepository(private val profileDao: ProfileDao) {

    private val _profileId: Long = 1 //TODO: Since only one profile at a time, consider moving this out of the database into local datastore
    private val _userProfile: MutableLiveData<Profile> =  MutableLiveData<Profile>()
    // in-memory cache of the Profile object
   val userProfile: LiveData<Profile> = _userProfile

    init{
        GlobalScope.launch(Dispatchers.Main.immediate){
            refreshData()
        }
    }

    // Call this from the Profile ViewModel to let the repository decide whether to use the Room data directly or first update from the API
    suspend fun refreshData() {
        //Make the network call and suspend execution till finished
        val result = try{
            makeProfileApiRequest(_profileId)
        } catch(e: Exception){
        }
        when (result){
            is Profile -> _userProfile.postValue(result)
            else -> if(!hasProfile) { // In-memory cache
                getProfile(_profileId) // Load database record
            }
            }
        }

    private suspend fun makeProfileApiRequest(profileId: Long? ) : Profile {
        return withContext(Dispatchers.IO){
            ProfileAPIServices.singleProfile(id = profileId ?: 1)
        }
    }
    private suspend fun getProfile(id: Long) {
        profileDao.getProfile(id).collect(){
            _userProfile.postValue(it)
        }
    }
    val hasProfile: Boolean
        get() = userProfile.value != null
}
