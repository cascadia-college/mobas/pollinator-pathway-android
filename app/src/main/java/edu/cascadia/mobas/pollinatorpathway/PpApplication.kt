package edu.cascadia.mobas.pollinatorpathway

import android.app.Application
import edu.cascadia.mobas.pollinatorpathway.data.database.PollinatorDao
import edu.cascadia.mobas.pollinatorpathway.data.datasource.LoginDataSource
import edu.cascadia.mobas.pollinatorpathway.data.repository.LoginRepository
import edu.cascadia.mobas.pollinatorpathway.data.repository.ProfileRepository
import edu.cascadia.mobas.pollinatorpathway.data.database.PpLocalDb
import edu.cascadia.mobas.pollinatorpathway.data.database.ProfileDao
import edu.cascadia.mobas.pollinatorpathway.data.repository.PollinatorRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob

class PpApplication:Application() {
    private val applicationScope = CoroutineScope(SupervisorJob())
    // Using by lazy so the database and the repository are only created when they're needed rather than when the application starts
    private val ppLocalDb:PpLocalDb by lazy {PpLocalDb.getDatabase(context = this, applicationScope)}
    private val loginDataSource: LoginDataSource by lazy { LoginDataSource(ppLocalDb.profileDao()) }
    //private val profileDataSource: ProfileDataSource by lazy { ProfileDataSource(ppLocalDb.profileDao()) }
    private val profileDao: ProfileDao by lazy {ppLocalDb.profileDao()}
    private val pollinatorDao: PollinatorDao by lazy {ppLocalDb.pollinatorDao()}
    val profileRepository: ProfileRepository by lazy { ProfileRepository(profileDao) }
    val loginRepository: LoginRepository by lazy { LoginRepository(loginDataSource) }
    val pollinatorRepository: PollinatorRepository by lazy { PollinatorRepository(pollinatorDao) }
}