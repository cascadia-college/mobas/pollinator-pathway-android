package edu.cascadia.mobas.pollinatorpathway.ui.registration

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.budiyev.android.codescanner.CodeScanner
import com.budiyev.android.codescanner.CodeScannerView
import com.budiyev.android.codescanner.DecodeCallback
import edu.cascadia.mobas.pollinatorpathway.R
import edu.cascadia.mobas.pollinatorpathway.data.database.ProfileDao
import edu.cascadia.mobas.pollinatorpathway.data.model.LoggedInUser
import edu.cascadia.mobas.pollinatorpathway.databinding.FragmentRegistrationMapBinding

class RegistrationMapFragment : Fragment() {

    private var _binding: FragmentRegistrationMapBinding? = null
    private val binding get() = _binding!!


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentRegistrationMapBinding.inflate(inflater, container, false)
        val root: View = binding.root
        val navController = findNavController()
        val activity = requireActivity()


        val QRCODE = getArguments()?.getString("QRCODE").toString()

        var businessNameText = root.findViewById<TextView>(R.id.textView27)
        var addressText = root.findViewById<TextView>(R.id.textView28)
        var emailText = root.findViewById<TextView>(R.id.textView29)

        var confirm = root.findViewById<Button>(R.id.button)
        var cancel = root.findViewById<Button>(R.id.button7)
        confirm.setOnClickListener{
            navController.navigate(R.id.navigation_reg_pub)
        }
        cancel.setOnClickListener{
            navController.navigate(R.id.navigation_home)
        }

        //getAccountInfo(email, password)



        return root
    }



}