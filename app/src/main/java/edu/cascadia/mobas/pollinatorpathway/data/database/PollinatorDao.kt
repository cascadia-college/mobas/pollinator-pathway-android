package edu.cascadia.mobas.pollinatorpathway.data.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import edu.cascadia.mobas.pollinatorpathway.data.model.Pollinator
import kotlinx.coroutines.flow.Flow

@Dao
interface PollinatorDao {
    @Query("SELECT * FROM Pollinator WHERE pollinatorId = :pollinatorId")
    fun getPollinator(pollinatorId: Long): Flow<Pollinator>

    @Query("SELECT * FROM Pollinator")
    fun getPollinators(): Flow<List<Pollinator>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addPollinator(pollinator: Pollinator) : Long
}