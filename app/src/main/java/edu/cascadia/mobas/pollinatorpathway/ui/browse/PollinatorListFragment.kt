package edu.cascadia.mobas.pollinatorpathway.ui.browse

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import edu.cascadia.mobas.pollinatorpathway.PpApplication
import edu.cascadia.mobas.pollinatorpathway.R
import edu.cascadia.mobas.pollinatorpathway.databinding.FragmentPollinatorListBinding


class PollinatorListFragment : Fragment() {

    private val pollinatorListViewModel: PollinatorListViewModel by viewModels {
        PollinatorListViewModelFactory((requireContext().applicationContext as PpApplication).pollinatorRepository)
    }
    private lateinit var binding: FragmentPollinatorListBinding
    private lateinit var adapter: PollinatorListAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentPollinatorListBinding.inflate(inflater, container, false)
        with(binding.recyclerView){
            setHasFixedSize(true)
            val divider = DividerItemDecoration(
                context, LinearLayoutManager(context).orientation
            )
            addItemDecoration(divider)
        }
        pollinatorListViewModel.pollinatorList.observe(viewLifecycleOwner, Observer {
            Log.i("noteLogging", it.toString())
            adapter = PollinatorListAdapter(it)
            binding.recyclerView.adapter = adapter
            binding.recyclerView.layoutManager = LinearLayoutManager(activity)
        })
        return binding.root
    }

}