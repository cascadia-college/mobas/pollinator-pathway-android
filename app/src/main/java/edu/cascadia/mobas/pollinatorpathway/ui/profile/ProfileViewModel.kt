package edu.cascadia.mobas.pollinatorpathway.ui.profile

import android.app.Application
import androidx.lifecycle.*
import edu.cascadia.mobas.pollinatorpathway.PpApplication
import edu.cascadia.mobas.pollinatorpathway.data.model.Profile
import edu.cascadia.mobas.pollinatorpathway.data.repository.ProfileRepository
import kotlinx.coroutines.launch

class ProfileViewModel(private val repository: ProfileRepository) : ViewModel()  {

    private val _profile = MutableLiveData<Profile>()
    val profile: LiveData<Profile> = repository.userProfile
    init{
        viewModelScope.launch{
            repository.refreshData()
        }}

}
class ProfileViewModelFactory(private val repository: ProfileRepository) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ProfileViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return ProfileViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}


