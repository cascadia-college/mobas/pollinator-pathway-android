package edu.cascadia.mobas.pollinatorpathway.data.database

import androidx.room.*
import edu.cascadia.mobas.pollinatorpathway.data.model.LoggedInUser
import kotlinx.coroutines.flow.Flow
import edu.cascadia.mobas.pollinatorpathway.data.model.Profile

@Dao
interface ProfileDao {
    @get:Query("SELECT * FROM profile_table ORDER BY email DESC")
    val profiles: List<Profile>

    @Query("SELECT * FROM profile_table WHERE profileId = :profileId")
    fun getProfile(profileId: Long): Flow<Profile>

    @Query("SELECT EXISTS(SELECT 1 FROM profile_table WHERE email = :email LIMIT 1)")
    fun emailExists(email: String): Boolean

    @Query("SELECT * FROM profile_table WHERE email = :email")
    fun getProfile(email: String): Flow<Profile>

    @Query("SELECT profileId as userId, email, password FROM profile_table WHERE email = :email AND password = :password")
    fun getLoggedInUser(email: String, password: String) : LoggedInUser

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertProfile(profile: Profile?): Long

    @Update
    suspend fun updateProfile(profile: Profile?)

    @Delete
    suspend fun deleteProfile(profile: Profile?)

    @Query("DELETE FROM profile_table")
    suspend fun deleteAll()
}