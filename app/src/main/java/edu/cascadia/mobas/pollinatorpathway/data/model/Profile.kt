package edu.cascadia.mobas.pollinatorpathway.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import edu.cascadia.mobas.pollinatorpathway.data.network.ProfileDTO
import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializable

@Serializable
@Entity(tableName = "profile_table")
data class Profile (
    // Ignored fields
    @ColumnInfo(name = "organization_name") var orgName: String? = null,
    @ColumnInfo(name = "org_type") var orgType: String? = null,
    @ColumnInfo(name = "org_description") var orgDescription: String? = null,
    @ColumnInfo(name = "website") var website: String? = null,
    @ColumnInfo(name = "facebook") var facebook: String? = null,
    @ColumnInfo(name = "instagram") var instagram: String? = null,
    @ColumnInfo(name = "date_joined") var dateJoined: String? = null,
    @ColumnInfo(name = "address") var address: String? = null,
    @ColumnInfo(name = "city") var city: String? = null,
    @ColumnInfo(name = "state") var state: String? = null,
    @ColumnInfo(name = "postal_code") var postalCode: String? = null,
    @ColumnInfo(name = "latitude") var latitude: Double? = null,
    @ColumnInfo(name = "longitude") var longitude: Double? = null,
    @ColumnInfo(name = "firstname") var firstname: String? = null,
    @ColumnInfo(name = "lastname") var lastname: String? = null,
    @ColumnInfo(name = "phone") var phone: String? = null,

    // Required fields
    @ColumnInfo(name = "email") var email: String,
    @ColumnInfo(name = "password") var password: String
) {
    @PrimaryKey(autoGenerate = true)
    var profileId: Long = 0L

    // New constructor that takes only email and password as arguments
    constructor(email: String, password: String) : this(
        "",
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        email,
        password
    )
}